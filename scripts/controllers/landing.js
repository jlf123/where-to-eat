/**
 * Created by john.flournoy on 9/22/16.
 */
'use strict';

angular.module('WhereToEat')
    .controller('LandingCtrl', ['$scope', 'Cuisines', 'LocationData', 'Locations', 'NgMap', '$compile', '$timeout', '$rootScope', '$uibModal',
        function($scope, Cuisines, LocationData, Locations, NgMap, $compile, $timeout, $rootScope, $uibModal){
            
            $scope.firstSearchMade = false;
            $scope.infowindow = null;
            $scope.cuisines = []
            $scope.marker = null;
            $scope.search = {
                type: [],
                distance: null,
                city: null
            }

            var restaurants = [];

            $scope.$on('POSITION-SET', function(){
                Cuisines.query({
                    lon: LocationData.getLocation().lon,
                    lat: LocationData.getLocation().lat
                }, function(response){
                    $scope.cuisines = response
                    $rootScope.$broadcast('Event:CuisinesLoaded', response)
                })

            })

            $scope.rating = null

            $scope.$on('Event:AppStarted', function(event, data){
                $scope.search = {
                    type: data.cuisines,
                    distance: data.distance.value,
                    city: data.city
                }
                $scope.getLocations()
                $rootScope.$broadcast('Event:firstSearchFinished')
                $scope.firstSearchMade = true;
            })

            $scope.getLocations = function () {
                if($scope.marker && $scope.infowindow){
                    resetMarker()
                }
                var tmp_ids = []
                angular.forEach($scope.search.type, function(item){
                    tmp_ids.push(item.cuisine.cuisine_id)
                })
                Locations.query({
                    lon: LocationData.getLocation().lon,
                    lat: LocationData.getLocation().lat,
                    cuisines: tmp_ids,
                    radius: $scope.search.distance * 1609.34
                }, function(response){
                    restaurants = response;
                    locationHelper()
                })
            }

            var locationHelper = function(){
                if(!restaurants.length){
                    $scope.showSearch()
                    $rootScope.$broadcast('event:no-results', 'There are no more locations of this cuisine type, please try a new search.')
                    return;
                }

                var index = getRandomIndex(restaurants.length),
                    target = restaurants[index]
                $scope.target = target;
                var center = {
                    lat: parseFloat(target.restaurant.location.latitude),
                    lng: parseFloat(target.restaurant.location.longitude)
                }
                NgMap.getMap().then(function(map){
                    var contentstring = '<restaurant-marker target="target" index="index"></restaurant-marker>';
                    var contentcompiled = $compile(contentstring)($scope)

                    $scope.rating = target.restaurant.user_rating.aggregate_rating;
                    var test = $compile('<input-stars max="5" ng-model="rating" ng-attr-readonly="true"></input-stars>')($scope)

                    $scope.infowindow = new google.maps.InfoWindow({
                        content: '<div id="marker-content"></div>'
                    });
                    $scope.marker = new google.maps.Marker({
                        position: center,
                        map: map,
                        title: 'Restaurant'
                    })
                    $scope.infowindow.open(map, $scope.marker);
                    $timeout(function(){
                        $('#marker-content').append(contentcompiled)
                        $('#stars').append(test)
                    }, 500)
                })
                
                
            }

            $scope.$on('Event:TryAgain', function(event, index){
                resetMarker()
                restaurants.splice(index, 1)
                locationHelper()
            })
            
            $scope.showSearch = function(){
                $scope.firstSearchMade = false;
                $rootScope.$broadcast('FilterSearch')
            }

            var resetMarker = function(){
                $scope.infowindow.close();
                $scope.marker.setMap(null)
            }

            var getRandomIndex = function(length){
                return parseInt(Math.random() * length)
            }

        }])

