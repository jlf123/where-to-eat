/**
 * Created by john.flournoy on 1/22/17.
 */
angular.module('WhereToEat').controller('ReviewModalCtrl', ['$scope', 'reviews', '$uibModalInstance',
    function($scope, reviews, $uibModalInstance){

        $scope.reviews = reviews

        $scope.close = function(){
            $uibModalInstance.dismiss();
        }

}])