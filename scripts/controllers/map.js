/**
 * Created by john.flournoy on 9/22/16.
 */
angular.module('WhereToEat')
    .controller('MapCtrl', ['$scope', 'NavigatorGeolocation', 'LocationData', 'NgMap', '$rootScope',
        function($scope, NavigatorGeolocation, LocationData, NgMap, $rootScope){

            $scope.cuisines = []
            $scope.firstSearchMade = false;
            
            $scope.$on('POSITION-SET', function(){
                NgMap.getMap().then(function(map){
                    var position = LocationData.getLocation()
                    map.setCenter(new google.maps.LatLng(position.lat, position.lon))
                    map.setZoom(10)
                })
            })

            $scope.map = {
                center: {lat: 37.090, lng: -95.712},
                zoom: 5
            }
            
            $scope.$on('Event:CuisinesLoaded', function(event, data){
                $scope.cuisines = data;
            })

            $scope.$on('Event:firstSearchFinished', function(){
                $scope.firstSearchMade = true;
            })

            $scope.$on('FilterSearch', function(){
                $scope.firstSearchMade = false;
            })

        }])