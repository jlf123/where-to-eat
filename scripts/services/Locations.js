'use strict';

angular.module('WhereToEat').factory('Locations', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/locations', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    })
}])