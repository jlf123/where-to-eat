'use strict';

angular.module('WhereToEat').factory('Reviews', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/reviews', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    })
}])