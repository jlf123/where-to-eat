'use strict';

angular.module('WhereToEat').factory('GoogleGeolocation', ['$resource', function($resource){
    return $resource('https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyC2HzRUHkD04fhFDIbIDZH-HX37Mg2UfAw');
}])