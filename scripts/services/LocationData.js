'use strict';

angular.module('WhereToEat').factory('LocationData', [function(){
    var location;
    
    var setlocation = function(lat,lon){
        location = {
            lat: lat,
            lon: lon
        }
    }
    
    var getLocation = function(){
        return location
    }
    
    return {
        setLocation: setlocation,
        getLocation: getLocation
    }
    
}])