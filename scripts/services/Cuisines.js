'use strict';

angular.module('WhereToEat').factory('Cuisines', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/cuisines', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    });
    
}])