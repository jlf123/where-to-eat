'use strict'

angular.module('WhereToEat').factory('Menu', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/menu', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    })
}])