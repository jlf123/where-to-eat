angular.module('WhereToEat').directive('tagDirective', function(){
    return {
        restrict: 'E',
        scope: {
            title: '='
        },
        templateUrl: '../views/tagdirective.html',
        link: function(scope, element, attrs){
            scope.close = function(){
                scope.$parent.removeCuisine(scope.title)
            }
            scope.isNumber = function(item){
                if(item){
                    return (typeof item === 'number')
                }
            }
        }
    }
    
})