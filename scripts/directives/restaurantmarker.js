/**
 * Created by john.flournoy on 1/19/17.
 */
angular.module('WhereToEat').directive('restaurantMarker',['$uibModal', 'Reviews', '$rootScope', function($uibModal, Reviews, $rootScope){
    return {
        restrict: 'E',
        scope: {
            target: '=',
            index: '@'
        },
        templateUrl: '../views/locationmarker.html',
        link: function(scope, element, attrs) {

            scope.tryAgain = function(){
                $rootScope.$broadcast('Event:TryAgain', scope.index)
            }

            scope.openReviews = function(restaurant){
                Reviews.query({
                    res_id: restaurant.id
                }, function(reviews){
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'reviewpaneltemplate.html',
                        size: 'md',
                        controller: 'ReviewModalCtrl',
                        resolve: {
                            reviews: function(){
                                return reviews;
                            }
                        }
                    })
                }, function(error){
                    console.log(error)
                    
                })
                
            }
        }
    }
}])