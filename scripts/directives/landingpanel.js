angular.module('WhereToEat').directive('landingPanel', [ '$rootScope', 'LocationData', 'GoogleGeolocation', '$timeout', 'screenSize',
    function($rootScope, LocationData, GoogleGeolocation, $timeout, screenSize){
        return {
            restrict: 'E',
            templateUrl: '../views/landingpanel.html',
            scope: {
                cuisines: '='
            },
            link: function(scope, element, attrs){

                var oldLocation = null;
                scope.isMobile = null
                scope.errorText = null;

                if (screenSize.is('xs')){
                    scope.isMobile = true;
                }

                screenSize.on('xs', function(isMatch){
                    scope.isMobile = isMatch
                })

                scope.search = {
                    cuisines: [],
                    city: 'Current Location'
                }

                scope.distances = [
                    { value: 10, label: '10 Miles Away' },
                    { value: 20, label: '20 Miles Away' },
                    { value: 30, label: '30 Miles Away' },
                    { value: 50, label: 'IDGAF' }
                ]

                scope.loading = false;
                
                scope.setTop = function(){
                    if(scope.isMobile){
                        return {
                            top: '5%'
                        }
                    } else {
                        return {
                            top: '10%'
                        }
                    }
                }

                scope.UpdateMap = function(){
                    //Wrap in a timeout to ensure that google autocomplete has updated ngmodel
                    $timeout(function(){
                        if(scope.search.city && oldLocation !== scope.search.city){
                            oldLocation = scope.search.city;
                            scope.loading = true;
                            if(scope.search.city === 'Current Location'){
                                GoogleGeolocation.save({}, function(position){
                                    setLocation(position.location.lat, position.location.lng)
                                })
                            } else {
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({ address : scope.search.city}, function(result, status){
                                    if(status == google.maps.GeocoderStatus.OK ){
                                        console.log(result)
                                        setLocation(result[0].geometry.location.lat(), result[0].geometry.location.lng())
                                    }
                                })
                            }
                        }
                    },100)

                }

                var setLocation = function(lat, lng){
                    LocationData.setLocation(lat, lng)
                    $rootScope.$broadcast('POSITION-SET')
                }

                scope.$on('Event:CuisinesLoaded', function(){
                    scope.loading = false;
                })

                scope.findFood = function(){
                    $rootScope.$broadcast('Event:AppStarted', scope.search)
                }

                scope.$on('Event:firstSearchFinished', function(){
                    scope.firstSearchMade = true;
                })

                scope.$on('FilterSearch', function(){
                    scope.firstSearchMade = false;
                })

                scope.$on('event:no-results', function(event, message){
                    scope.errorText = message;
                })

            }
        }
}])