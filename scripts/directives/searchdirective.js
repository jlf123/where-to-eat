angular.module('WhereToEat').directive('searchDirective',function(){
    return {
        restrict: 'E',
        scope: {
            cuisines: '=',
            distance: '=',
            city: '='
        },
        replace: true,
        templateUrl: '../views/searchdirective.html',
        link: function(scope, element, attrs){
            scope.removeCuisine = function(title){
                for(var i = 0; i < scope.cuisines.length; i++){
                    if(scope.cuisines[i].cuisine.cuisine_name === title){
                        scope.cuisines.splice(i, 1);
                    }
                }
            }
        }

    }

})