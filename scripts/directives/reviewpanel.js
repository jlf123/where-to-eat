/**
 * Created by john.flournoy on 1/21/17.
 */
angular.module('WhereToEat').directive('reviewPanel', [ '$uibModal', function($uibModal){
    return {
        restrict: 'E',
        scope: {
            reviews: '='
        },
        templateUrl: '../views/reviewpanel.html'
    }

}])