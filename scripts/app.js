/**
 * Created by john.flournoy on 9/22/16.
 */
angular.module('WhereToEat', [
    'ui.router',
    'ngAnimate',
    'ui.bootstrap',
    'ngResource',
    'ngMap',
    'angular-input-stars',
    'ui.select',
    'matchMedia'
])
.constant('zomato_uri', 'https://developers.zomato.com/api/v2.1/')
    
.constant('BASE_URI', 'http://restaurants2try.com/r2t/v1')

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/');
    

    $stateProvider
        .state('home', {
            url: '/',
            views: {
                header: {
                    templateUrl: 'views/landing.html',
                    controller: 'LandingCtrl'
                },
                map: {
                    templateUrl: 'views/map.html',
                    controller: 'MapCtrl'
                }
            }
        })

}])

//curl 'https://developers.zomato.com/api/v2.1/categories' -H 'Origin: http://localhost:3000' -H 'user-key: 7d41e8b81881a80c73ce44955202960f' -H 'Accept-Encoding: gzip, deflate, sdch' -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36' -H 'Accept: application/json' -H 'Referer: http://localhost:3000'  -H 'Connection: keep-alive' --compressed