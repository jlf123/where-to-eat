/**
 * Created by john.flournoy on 9/22/16.
 */
'use strict';

module.exports = function(grunt){

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        connect: {
            server: {
                options: {
                    hostname: '0.0.0.0',
                    port: 3000,
                    livereload: true
                },
                proxies: [
                    {
                        context: '/r2t/v1',
                        host: 'restaurants2try.com',
                        port: 8080,
                        https: false,
                        protocol: 'https:',
                        xforward: true
                    }
                ]
            }
        },

        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        'dist/{,*/}*'
                    ]
                }]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: 'dist'
                }]
            }
        },

        ngtemplates: {
            app: {
                src: ['views/{,*/}*.html'],
                dest: 'dist/scripts/templates.js',
                options: {
                    prefix: '',
                    module: 'fcpApp',
                    htmlmin: '<%= htmlmin.app %>'
                }
            }
        },

        useminPrepare: {
            html: 'index.html',
            options: {
                dest: 'dist',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        usemin: {
            html: ['dist/{,*/}*.html'],
            css: ['dist/styles/{,*/}*.css'],
            options: {
                assetsDirs: ['dist','dist/images']
            }
        },

        filerev: {
            dist: {
                src: [
                    'dist/scripts/{,*/}*.js',
                    'dist/styles/{,*/}*.css'
                ]
            }
        },

        watch: {
            scripts: {
                files: ['scripts/**/*.js', 'styles/*', 'views/*', 'index.html'],
                task: ['concat']
            },
            options: {
                spawn: false,
                livereload: true
            }
        },

        compress: {
            options: {
                mode: 'tgz'
            },
            files: [
                {
                    cwd: 'dist',
                    src: ['**/*'],
                    expand: true
                }
            ],
            build: {
                options: {
                    archive: 'wheretoeat.tgz'
                },
                files: [
                    {
                        cwd: 'dist',
                        src: [ '**/*' ],
                        expand: true
                    }
                ]
            },
        },

        copy: {
            dist: {
                files: [
                    { expand: true, dest: 'dist', src: ['views/{,*/}*.html', '*.html'] },
                    { expand: true, cwd: 'images', dest: 'dist/images', src: '**/*' }
                ]
            },
            styles: { expand: true, cwd: 'style', dest: 'dist/styles', src: '*.css' }
        },

        wiredep: {
            task: {
                src: 'index.html'
            }

        }
        
    })

    grunt.registerTask('default', ['wiredep','connect', 'watch'])

    grunt.registerTask('build', [
        'clean:dist',
        'wiredep',
        'useminPrepare',
        'ngtemplates',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin',
        'compress:build'
    ])
}